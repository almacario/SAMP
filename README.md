Here you can find the most successful works done on SAMP - Everything is in ITALIAN

Programming language: PAWN

Link on Youtube tutorials done: http://yt.vu/p/PL4aIQp4LunbOsurZExVUlLTZyYr1qNxUZ

<br />
<br />

Panama City Roleplay gamemode 

Original upload on SAMP community forum: http://forum.sa-mp.com/showthread.php?t=284058

Server opened in 2010 with an average # of players online: 35 peak: >70

Current # of downloads: 3200

    Programmer: Alberto Macario(nickname Vez Vialpando)
    Base: Carlito's Roleplay
    Script Lines: 26.000
    
    [MAIN SYSTEMS]
    
        :: Dynamic Factions System ::
        :: Dynamic House System ::
        :: Dynamic Business System ::
        :: Dynamic Buildings System ::
        :: Dynamic Robbery System ::
        :: Dynamic Cars System ::
        :: Dynamic Owner Cars System :: 
    
    [OTHERS]
        
        :: Jobs ::
        -Mechanic Job
        -Drugs Dealer Job
        -Mats Dealer Job
        -Instructor(license) Job
        -Products Dealer Job
        -Detective Job
        -Barrister Job
        
        :: Missions ::
        -Sweeper
        -Sandwiches Seller
        -Ice-Cream man
        And Others
        
        :: Admin System ::
        -Very Easy
        
        :: Anti Metagame System ::
        -Now players can't see the name of the other player on the head, but if you are on Admin Duty you can.
        
        :: Weapon Saving ::
        -When you disconnect from server, it save your weapon
        
        :: Login And Register ::
        -Obviously in Dialog
        
        :: Tutorial ::
        -A simple tutorial with the more important point of the city.
        
        :: Dialog Menu ::
        -Dialog for House
        -Dialog for Business
        -Dialog for Phone
        
        :: Professional View ::
        -Professional view for:
        .House
        .Business
        .Buildings
        
        :: Easy Commands ::
        -The commands are simple, for example you can use buy command(/compra) inside Ammunation, 24/7, and more
        
        :: Business ::
        -Ammunation(Armerie)
        -24/7(Emporio)
        -Electronic Shop(Elettronica)
        -Phone Shop(Telefonia)
        -Advertising(Pubblicità)
        -Bar/Pub
        -Restaurant(Ristorante)
        -Clothes Shop(Vestiario)
        -Betting Shop(Negozio di scommesse)
        
        :: Intelligent PayDay ::
        -The PayDay is realistic, for legal factions, they takes some money into their Faction-Bank
        
        :: AdminDuty ::
        -AdminDuty is beautiful, admins take a beautiful parrot on the shouder(Funny )
        
        :: Bank & ATM ::
        -The bank and ATM are in Dialog, you can "Withdraw, Deposit, See the balance, take a loan and repay it"
        
        :: Clothes System ::
        -You change the clothes with right and left mouse button
        
        :: 3D Textdraws ::
        -3DTextdraws are very present inside the server, but they are very professional and beautiful
        
        :: Rope S.W.A.T. ::
        -Thanks: Al Morelli, you can use the rope swat for mission and scenes roleplay.
        
        :: Saving Location ::
        -Save the location when player disconnect
        
        :: GPS System ::
        -GPS give you the most important place of the city(Menu)
        
        :: Anims ::
        -The server has a lot of Anims.
        
        :: Health System ::
        -When the player is deathing appear a message with a injured anim
        
        :: Faction Storage System ::
        -Safe storage of factions mats and drugs
        
        :: Walkie Talkie System ::
        -For more Roleplay I add a Walkie Talkie system and remove Factions(No-RP) chat, there are 100.000 frequense
        
        :: Roleplay Bot ::
        -Two Roleplay Bot, the first is a police bot, the second is a Bank Bot
        
        :: Dynamic Robbery System ::
        -You can create a Robbery place in the game, I'm the first scripter for that
        
        And More... 
    
    
Blackshark gamemode

    main scripter: Cesar Wialpont
    another successful gamemode with over 2900 downloads, basically I was the supervisor during the development, more information on Google